package hu.braininghub.bh04.contact.dao;

import java.util.List;

import hu.braininghub.bh04.contact.model.Contact;

public interface ContactDAO {

	
	void persist(Contact c);
	
	Contact getByFirstName(String firstName);
	
	List<Contact> getAll();
}
