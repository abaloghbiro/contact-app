package hu.braininghub.bh04.contact.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.Dependent;

import hu.braininghub.bh04.contact.model.Contact;

@Dependent
public class DefaultContactDAO implements ContactDAO {

	private final Map<String, Contact> CACHE = new HashMap<String, Contact>();

	
	public void persist(Contact c) {
		CACHE.put(c.getFirstName(), c);
	}

	public Contact getByFirstName(String firstName) {
		return CACHE.get(firstName);
	}

	public List<Contact> getAll() {
		return new ArrayList<Contact>(CACHE.values());
	}

}
