package hu.braininghub.bh04.contact.service;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.inject.Inject;

import hu.braininghub.bh04.contact.dao.ContactDAO;
import hu.braininghub.bh04.contact.model.Contact;

@Stateless
public class DefaultContactService implements ContactService, RemoteContactService {

	@Inject
	private ContactDAO dao;
	
	@Resource
	private EJBContext ctx;
	
	
	@PostConstruct
	public void init() {
		System.out.println("Contact service postconstruct : "+dao.toString());
		System.out.println("EJB ctx ctdata : "+ctx.getContextData().values());
	}
	
	@PreDestroy
	public void destroy() {
		System.out.println("Contact service preDestroy : "+dao.toString());
	}
	
	public List<Contact> getAllContact() {
		return dao.getAll();
	}

	public Contact contactByFirstName(String firstName) {
		return dao.getByFirstName(firstName);
	}

	public void addContact(Contact c) {
		dao.persist(c);
	}

	

}
