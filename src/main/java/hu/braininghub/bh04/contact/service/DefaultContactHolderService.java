package hu.braininghub.bh04.contact.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.PostActivate;
import javax.ejb.PrePassivate;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.ejb.StatefulTimeout;

import hu.braininghub.bh04.contact.model.Contact;

@Stateful
@StatefulTimeout(value = 10, unit = TimeUnit.MINUTES)
public class DefaultContactHolderService implements ContactHolderService {

	private List<Contact> contacts;

	private Integer beanId;

	@PostConstruct
	public void init() {
		contacts = new ArrayList<>();
	}

	@PreDestroy
	public void destroy() {
		System.out.println("@PreDestroy invoked, bean id: " + beanId);
	}

	@PrePassivate
	public void prePassivate() {
		System.out.println("@PrePassivate invoked, bean id: " + beanId);
	}

	@PostActivate
	public void postActivate() {
		System.out.println("@PostActivate invoked, bean id: " + beanId);
	}

	@Override
	public List<Contact> getAllContact() {
		return contacts;
	}

	@Override
	public List<Contact> contactByFirstName(String firstName) {

		return contacts.stream().filter(c -> c.getFirstName().equalsIgnoreCase(firstName)).collect(Collectors.toList());

	}

	@Override
	public void addContact(Contact c) {
		contacts.add(c);
	}
	
	public void setBeanId(Integer beanId) {
		this.beanId = beanId;
	}

	@Override
	public Integer getBeanId() {
		return beanId;
	}

	@Remove
	@Override
	public void removeInstance() {
		System.out.println("@Remove invoked, bean id: " + beanId);
	}


}
