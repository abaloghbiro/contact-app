package hu.braininghub.bh04.contact.service;

import java.util.List;

import javax.ejb.Local;

import hu.braininghub.bh04.contact.model.Contact;

@Local
public interface ContactHolderService {

	List<Contact> getAllContact();
	
	List<Contact> contactByFirstName(String firstName);
	
	void removeInstance();
	
	void addContact(Contact c);
	
	Integer getBeanId();
	
	void setBeanId(Integer beanId);
}
