package hu.braininghub.bh04.contact.client;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;

import hu.braininghub.bh04.contact.model.Contact;
import hu.braininghub.bh04.contact.service.RemoteContactService;

public class AppClient {
    public static void main(String[] args) {

        try {

            Properties props = new Properties();
            props.setProperty("java.naming.factory.initial", "com.sun.enterprise.naming.SerialInitContextFactory");
            props.setProperty("java.naming.factory.url.pkgs", "com.sun.enterprise.naming");
            props.setProperty("java.naming.factory.state", "com.sun.corba.ee.impl.presentation.rmi.JNDIStateFactoryImpl");
            props.setProperty("org.omg.CORBA.ORBInitialHost", "localhost");
            props.setProperty("org.omg.CORBA.ORBInitialPort", "3700");

            Context ctx = new InitialContext(props);
            RemoteContactService service = (RemoteContactService) ctx.lookup("java:global/contact/DefaultContactService!hu.braininghub.bh04.contact.service.RemoteContactService");

            Contact c = new Contact();
            c.setEmail("attila.balogh-biro@ui.city");
            c.setFirstName("Attila");
            c.setLastName("Balogh-Biro");
            c.setPhoneNumber("+36709074129");
            
            service.addContact(c);
            
            System.out.println(service.getAllContact());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}  