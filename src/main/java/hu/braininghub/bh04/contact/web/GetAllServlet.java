package hu.braininghub.bh04.contact.web;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hu.braininghub.bh04.contact.model.Contact;
import hu.braininghub.bh04.contact.service.ContactService;

@WebServlet(urlPatterns = { "/getAll" })
public class GetAllServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	private ContactService service;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		for (Contact c : service.getAllContact()) {

			resp.getWriter().println("Contact : " + c);
		}

	}

}
