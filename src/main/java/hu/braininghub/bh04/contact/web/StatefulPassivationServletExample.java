package hu.braininghub.bh04.contact.web;

import java.io.IOException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hu.braininghub.bh04.contact.service.ContactHolderService;

@WebServlet(urlPatterns = { "/passivation" })
public class StatefulPassivationServletExample extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String CONTACT_HOLDER_SESSION_KEY = "contactHolder";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String beanCountparam = req.getParameter("count");

		InitialContext ic = null;
		try {
			ic = new InitialContext();
		} catch (NamingException e1) {
			e1.printStackTrace();
		}

		if (beanCountparam != null) {

			int beanCount = Integer.parseInt(beanCountparam);

			try {
				for (int i = 0; i < beanCount; i++) {

					ContactHolderService passivation = (ContactHolderService) ic
							.lookup("java:global/contact/DefaultContactHolderService"
									+ "!hu.braininghub.bh04.contact.service.ContactHolderService");
					passivation.setBeanId(i);

					req.getSession().setAttribute(CONTACT_HOLDER_SESSION_KEY + passivation.getBeanId(), passivation);

					resp.getWriter().println("-- Created bean with id is : " + passivation.getBeanId());
				}
			} catch (Exception e) {
				throw new ServletException(e);
			}
		}

		String beanActivationIndex = req.getParameter("activate");

		if (beanActivationIndex != null) {
			try {

				ContactHolderService passivation = (ContactHolderService) req.getSession()
						.getAttribute(CONTACT_HOLDER_SESSION_KEY + beanActivationIndex);

				System.out.println("Bean id is :" + passivation.getBeanId());

				resp.getWriter().println("Activated bean with id is : " + passivation.getBeanId());

			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
	}

}
